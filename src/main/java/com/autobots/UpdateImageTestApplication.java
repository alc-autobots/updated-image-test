package com.autobots;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpdateImageTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpdateImageTestApplication.class, args);
	}

}
