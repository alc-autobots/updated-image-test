package com.autobots.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/home")
public class UpdateImage {

    @GetMapping("/")
    public String imageUpdated() {
        return "Working...";
    }
}
