capture git revision number & use it for docker tag: 
docker tag <image> <image>:$(git rev-parse --short HEAD)"

https://github.com/spotify/dockerfile-maven/blob/master/docs/authentication.md


mvn clean package -Drevision=<versionNO> -Ddockerfile.useMavenSettingsForAuth=true dockerfile:push